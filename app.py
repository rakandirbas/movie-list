# -*- coding: utf-8 -*-
"""
This module is a web-app that returns a page containing all movies and their people from
[Studio Ghibli](https://ghibliapi.herokuapp.com/).
"""


import os
import logging
import web
from services import MoviesFetcher


class MoviesHandler:
    def GET(self):
        return web.render.movies(web.movies_fetcher.cache)


if __name__ == '__main__':
    LOG_LEVEL = os.environ['LOG_LEVEL']
    logging.basicConfig(level=LOG_LEVEL)

    ghibli_api_base_url = os.environ['GHIBLI_API_BASE_URL']
    retries = os.environ['RETRIES']
    refresh_delay = os.environ['REFRESH_DELAY']
    web.movies_fetcher = MoviesFetcher(ghibli_api_base_url, retries, refresh_delay)

    urls = (
        '/movies', 'MoviesHandler'
    )

    web.render = web.template.render('templates/')

    app = web.application(urls, globals())
    app.run()
