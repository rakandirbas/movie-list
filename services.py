import logging
import time
import threading
import requests
from requests.exceptions import RequestException
from models import Movie
from models import Person


class MoviesFetcher:
    """
    A service that fetches and caches Movies from Studio Ghibli.
    """

    def __init__(self, base_api_url, fetch_retries=3, refresh_delay=60):
        self.logger = logging.getLogger('MoviesFetcher')
        self.base_api_url = base_api_url
        self.fetch_retries = int(fetch_retries)
        self.refresh_delay = int(refresh_delay)
        self.cache = {}
        self._refresh_cache()

    def _refresh_cache(self):
        self.logger.info("Started fetching movies.")
        films_json = self._fetch_entity_data("films")
        people_json = self._fetch_entity_data("people")
        if films_json and people_json:
            self.cache = self._construct_cache(films_json, people_json)
            threading.Timer(self.refresh_delay, self._refresh_cache).start()
        self.logger.info("Done fetching %d movies.", len(self.cache))

    def _fetch_entity_data(self, entity):
        url = self.base_api_url + "/" + entity
        for attempt_counter in range(self.fetch_retries):
            try:
                response = requests.get(url)
                if response.status_code == 200:
                    return response.json()
            except RequestException as e:
                self.logger.exception("Error querying entity's URL: %s", url, e)
            time.sleep((attempt_counter+1) * self.fetch_retries)

        return None

    @staticmethod
    def _construct_cache(films_json, people_json):
        cache = {}

        for film_json in films_json:
            id_ = film_json['id']
            title = film_json['title']
            desc = film_json['description']
            movie = Movie(id_, title, desc)
            cache[id_] = movie

        for person_json in people_json:
            id_ = person_json['id']
            name = person_json['name']
            person = Person(id_, name)
            for film_url in person_json['films']:
                film_id = film_url[film_url.rfind('/') + 1:]
                if film_id in cache:
                    cache[film_id].add_person(person)

        return cache
