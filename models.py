class Movie:
    def __init__(self, id_, title, desc):
        self.id = id_
        self.title = title
        self.desc = desc
        self.people = []

    def add_person(self, person):
        self.people.append(person)


class Person:
    def __init__(self, id_, name):
        self.id = id_
        self.name = name
