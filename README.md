# Movie List

A simple web app to show movies and their characters from  [Studio Ghibli](https://ghibliapi.herokuapp.com/).

# How to run app & tests locally
 
* Create a copy of `.env.template` as `.env` and fill-in the values of the env vars as necessary.
* `docker-compose up app`
* Visit http://localhost:8000/movies to check the newest movies :)

To run the tests simply do `docker-compose up tests`.

The entry-point is `app.py` if you want to inspect the code ;)

# Disclaimer

I'm a java dev and tried my best to build the app according to python's best-practices. However, I may have missed 
cleaner pythonic ways of doing things due to my lack of python knowledge, so apologies for that :).