import unittest
from unittest.mock import patch
from unittest.mock import Mock
from unittest import mock
from services import MoviesFetcher


class TestMoviesCache(unittest.TestCase):
    base_api_url = "http://mock.com"

    @patch("services.threading")
    @patch("services.requests")
    def test_movies_cache_fetches_and_parses_data_correctly(self, requests_mock, threading_mock):
        expected_film_id = "film_id"
        expected_film_title = "film_title"
        expected_film_desc = "film_desc"

        expected_film = {"id": expected_film_id, "title": expected_film_title, "description": expected_film_desc}
        expected_films = [expected_film]

        expected_person_id = "person_id"
        expected_person_name = "person_name"
        expected_person_films = ["/" + expected_film_id]
        expected_person = {"id": expected_person_id, "name": expected_person_name, "films": expected_person_films}
        expected_people = [expected_person]

        films_response_mock = Mock()
        films_response_mock.status_code = 200
        films_response_mock.json.return_value = expected_films

        people_response_mock = Mock()
        people_response_mock.status_code = 200
        people_response_mock.json.return_value = expected_people

        requests_mock.get.side_effect = [films_response_mock, people_response_mock]

        timer_mock = Mock()
        threading_mock.Timer.return_value = timer_mock

        movies_fetcher = MoviesFetcher(TestMoviesCache.base_api_url)

        self.assertIn(mock.call(TestMoviesCache.base_api_url + "/" + "films"), requests_mock.get.call_args_list)
        self.assertIn(mock.call(TestMoviesCache.base_api_url + "/" + "people"), requests_mock.get.call_args_list)

        cached_movie = movies_fetcher.cache[expected_film_id]
        self.assertEqual(cached_movie.id, expected_film_id)
        self.assertEqual(cached_movie.title, expected_film_title)
        self.assertEqual(cached_movie.desc, expected_film_desc)
        self.assertEqual(len(cached_movie.people), 1)
        self.assertEqual(cached_movie.people[0].id, expected_person_id)
        self.assertEqual(cached_movie.people[0].name, expected_person_name)

        threading_mock.Timer.assert_called()
        timer_mock.start.assert_called()

#     TODO: add a test case when requests.get() throws an Exception
#     TODO: add a test case when requests.get() returns a response code that's not 200
#     TODO: add a test case to test retries

#     I'm really really sorry not to finish the tests but I had to do this quickly before work and try to get it done.
#     However, i'm gonna explain how I would do them:
#     All of the previous TODO test cases can be done by setting the requests_mock.get.side_effect to a list
#     that has an Exception, a response with a status code that is not 200, a proper response,
#     and then we mock/patch time.sleep and assert that requests.get() was called the correct number of times
#     with that time.sleep was also called correct number of times with the right parameters.


if __name__ == '__main__':
    unittest.main()
